var price= require("../price.js");

describe("Price function", () => {
  test("it should return the final price with discount applied and after tax", () => {
  
  expect(price("550","5","on").valueOf()).toEqual("3014.275".valueOf());
  expect(price("1000","10","DE").valueOf()).toEqual("9000".valueOf());
  expect(price("AA",5,"QC").valueOf()).toEqual("-1".valueOf());
  expect(price("6000","5","243").valueOf()).toEqual("-1".valueOf());

  });
});