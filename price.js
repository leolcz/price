const Big = require('big.js');
const DISCOUNT_RATES={
    "1000":"0.03",
    "5000":"0.05",
    "7000":"0.07",
    "10000":"0.1"
};

const TAXES={
    "AB":"0.05",
    "ON":"0.13",
    "QC":"0.14975",
    "MI":"0.06",
    "DE":"0"
};

module.exports= function(number,pricePerItem,province){
	var n,p,tax;
	try{
		n=new Big(number);
		p=new Big(pricePerItem);
		tax=new Big(TAXES[province.toUpperCase()]);
	}catch{
		console.log("Invalid input parameters: "+number+","+pricePerItem+","+province);
		return "-1";
	}
    var priceBeforeDiscount=n.times(p);
    var discount=new Big(0);
    for (var key in DISCOUNT_RATES){
        if(priceBeforeDiscount.gt(new Big(key)) || priceBeforeDiscount.eq(new Big(key))){
            discount=new Big(DISCOUNT_RATES[key]);
        }
    }
    return priceBeforeDiscount.times(new Big(1).minus(discount)).times(new Big(1).plus(tax));
}
